<?php
/*
 (C) Vladimir Smagin, 2015
 http://blindage.org
 21h@blindage.org

 Project site: http://php-gnupg.blindage.org/
*/
class GnupgSimple {

	private $gnupg_res;

	function  __construct($pubkey, $seckey, $passwd) {
		$this->gnupg_res = gnupg_init();
		if ($seckey<>'') gnupg_import($this->gnupg_res, $seckey);
		if ($pubkey<>'') gnupg_import($this->gnupg_res, $pubkey);
		$keyinfo=gnupg_keyinfo($this->gnupg_res,'');
		foreach ($keyinfo[0]['subkeys'] as $subkey) {
			if ($subkey['can_sign']==1) { $fingerprint_sign = $subkey['fingerprint']; }
			if ($subkey['can_encrypt']==1) { $fingerprint_encrypt = $subkey['fingerprint']; }
		}
		gnupg_addsignkey($this->gnupg_res,$fingerprint_sign, $passwd);
		gnupg_addencryptkey($this->gnupg_res, $fingerprint_encrypt);
		gnupg_adddecryptkey($this->gnupg_res,$fingerprint_sign, $passwd);
	}

	function sign_clear($text) {
		gnupg_setsignmode($this->gnupg_res,GNUPG_SIG_MODE_CLEAR);
		return gnupg_sign($this->gnupg_res,$text);
	}
	
	function sign_detached($text) {
		gnupg_setsignmode($this->gnupg_res,GNUPG_SIG_MODE_DETACH);
		return gnupg_sign($this->gnupg_res,$text);
	}
	
	function encrypt($text) {
		return gnupg_encrypt($this->gnupg_res, $text);
	}
	function encrypt_and_sign($text) {
		return gnupg_encryptsign($this->gnupg_res, $text);
	}
	function decrypt($text) {
		return gnupg_decrypt($this->gnupg_res,$text);
	}
	function verify_sign($text) {
		return gnupg_verify($this->gnupg_res,$text,false)[0]['fingerprint'];
	}
	function verify_detached_sign($text, $sign) {
		return gnupg_verify($this->gnupg_res,$text,$sign)[0]['fingerprint'];
	}
	function mail($recipient, $sender, $title, $text) {
		$headers = 'From: '. $sender . "\r\n" .
			'Reply-To: '. $sender . "\r\n" .
			'X-Mailer: PHP/' . phpversion();
		return mail($recipient, $title, $text, $headers);
	}
}
/*
$pubkey=file_get_contents('../gpgkeys/pubkey.asc');
$seckey=file_get_contents('../gpgkeys/seckey.asc');
$gpg = new GnupgSimple($pubkey, $seckey, 'tormoz');

$text = file_get_contents('test2text.txt');
var_dump($text);

$sign1 = $gpg->sign_detached($text);
var_dump($sign1);
file_put_contents('test2sign_d.asc',$sign1);

$sign2 = $gpg->sign_clear($text);
var_dump($sign2);
file_put_contents('test2sign_c.asc',$sign2);

echo "Detached sign: ";
var_dump($gpg->verify_detached_sign(file_get_contents('test2text.txt'),file_get_contents('test2sign_d.asc')));

echo "Clear sign: ";
var_dump($gpg->verify_sign(file_get_contents('test2sign_c.asc')));

var_dump($gpg->mail('21h@blindage.org', 'admin@radio70.ru', 'Test signed message', $sign2));
*/
?>
